#include <ros/ros.h>
#include "ompl/base/spaces/DubinsStateSpace.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "ompl/base/ScopedState.h"
#include "ompl/base/objectives/PathLengthOptimizationObjective.h"
#include <ompl/base/objectives/VFUpstreamCriterionOptimizationObjective.h>
#include "ompl/geometric/PathSimplifier.h"
#include "ompl/base/DiscreteMotionValidator.h"
#include "ompl/geometric/planners/rrt/RRTstar.h"
#include "ompl/geometric/planners/rrt/RRTConnect.h"
#include "ompl/geometric/planners/rrt/pRRT.h"
#include "ompl/geometric/planners/rrt/VFRRT.h"
#include "ompl_rviz/ompl_rviz.h"

namespace ob = ompl::base;
namespace og = ompl::geometric;

/** Gradient of the potential function 1 + sin(x[0]) * sin(x[1]). */
Eigen::VectorXd field(const ob::State *state)
{
    const ob::RealVectorStateSpace::StateType &s = *state->as<ob::RealVectorStateSpace::StateType>();
    Eigen::VectorXd v(2);
//     v[0] = std::cos(x[0]) * std::sin(x[1]);
//     v[1] = std::sin(x[0]) * std::cos(x[1]);
//     return -v; //example_field
     
//      double x=s.values[0];
//      double y=s.values[1];
//      
//      double x3=x*x*x;
//      double y3=y*y*y; 
//      double fi=x3*x+y3*y-160000.0;
//      Eigen::VectorXd gradfi(2);
//      gradfi[0] = 4.0*x3;
//      gradfi[1] = 4.0*y3;
//      Eigen::VectorXd gradHfi(2);
//      gradHfi[0] = -gradfi[1];
//      gradHfi[1] = gradfi[0];
//      double G = -0.636619772367581*atan(10*fi);
//      double H = -sqrt(1.0-G*G);
//      double h = 1.0/gradfi.norm();
//  
//      gradfi.normalize();  	
//      gradfi = gradfi*(0.001*G);
//      gradHfi = gradHfi*(H*h);
//      
//      Eigen::VectorXd v2=gradfi + gradHfi;
//      v2.normalize();
//      return v2;  //circular_field
     
     double d0=5;
     double a=0.3;
     double l=40;
     v[0]=1;
     v[1]=a*((d0-l/2)-s.values[1]);
     v.normalize();
     return v; //corridor_field
}


// Our collision checker. For this demo, our robot's state space
// lies in [0,1]x[0,1], with a circular obstacle of radius 0.25
// centered at (0.5,0.5). Any states lying in this circular region are
// considered "in collision".
class ValidityChecker : public ob::StateValidityChecker
{
public:
    ValidityChecker(const ob::SpaceInformationPtr& si) :
        ob::StateValidityChecker(si) {}
    // Returns whether the given state's position overlaps the
    // circular obstacle
    bool isValid(const ob::State* state) const
    {
        return this->clearance(state) > 0.0;
    }
    // Returns the distance from the given state's position to the
    // boundary of the circular obstacle.
    double clearance(const ob::State* state) const
    {
        // We know we're working with a RealVectorStateSpace in this
        // example, so we downcast state into the specific type.
        const ob::RealVectorStateSpace::StateType* state2D =
            state->as<ob::RealVectorStateSpace::StateType>();
        // Extract the robot's (x,y) position from its state
        double x = state2D->values[0];
        double y = state2D->values[1];
        // Distance formula between two points, offset by the circle's
        // radius
        return sqrt((x-2.5)*(x-2.5) + (y-3.5)*(y-3.5)) - 0.0;
    }
};


// Returns a structure representing the optimization objective to use
// for optimal motion planning. This method returns an objective which
// attempts to minimize the length in configuration space of computed
// paths.
ob::OptimizationObjectivePtr getPathLengthObjective(const ob::SpaceInformationPtr& si)
{
    return ob::OptimizationObjectivePtr(new ob::PathLengthOptimizationObjective(si));
}



int main(int argc, char **argv) {
  ros::init(argc, argv, "ompl_rviz");
  ros::NodeHandle n("~");

  ros::Publisher pub_path_marker = n.advertise<visualization_msgs::Marker>("path", 1);
  ros::Publisher pub_graph_marker = n.advertise<visualization_msgs::Marker>("graph", 1);
  ros::Duration(1.0).sleep();

  // Construct the robot state space in which we're planning. We're
  // planning in [0,1]x[0,1], a subset of R^2.
  ob::StateSpacePtr space(new ob::RealVectorStateSpace(2));

  // Set the bounds of space   
  ob::RealVectorBounds bounds(2);
  bounds.setLow(0, -30);  // x
  bounds.setLow(1, -30); // y 
  bounds.setHigh(0, 30); // x
  bounds.setHigh(1, 30); // y
  space->as<ob::RealVectorStateSpace>()->setBounds(bounds);

  // Construct a space information instance for this state space
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));

  // Set the object used to check which states in the space are valid
  si->setStateValidityChecker(ob::StateValidityCheckerPtr(new ValidityChecker(si)));
  //si->setStateValidityCheckingResolution(0.0001);
  si->setup();

  // Set our robot's starting state to be the bottom-left corner of
  // the environment, or (0,0).
  ob::ScopedState<> start(space);
  start->as<ob::RealVectorStateSpace::StateType>()->values[0] = -20.0;
  start->as<ob::RealVectorStateSpace::StateType>()->values[1] = -20.0;

  // Set our robot's goal state to be the top-right corner of the
  // environment, or (1,1).
  ob::ScopedState<> goal(space);
  goal->as<ob::RealVectorStateSpace::StateType>()->values[0] = 20.0;
  goal->as<ob::RealVectorStateSpace::StateType>()->values[1] = -20.0;
 
  // Create a problem instance
  ob::ProblemDefinitionPtr pdef(new ob::ProblemDefinition(si));
 
  // Set the start and goal states
  pdef->setStartAndGoalStates(start, goal, 0.1);

  // Set the objective function
  //pdef->setOptimizationObjective(getPathLengthObjective(si));
  pdef->setOptimizationObjective(std::make_shared<ob::VFUpstreamCriterionOptimizationObjective>(si, field));

  // Construct our optimizing planner using the VFRRT algorithm.
 // double explorationSetting = 0.7;
 // double lambda = 1;
 // unsigned int update_freq = 100;
 // ob::PlannerPtr optimizingPlanner(new og::VFRRT(si, field, explorationSetting, lambda, update_freq));

 // Construct our optimizing planner using the RRTstar algorithm.
  ob::PlannerPtr optimizingPlanner(new og::RRTstar(si));
 
  // Set the problem instance for our planner to solve
  optimizingPlanner->setProblemDefinition(pdef);
  optimizingPlanner->setup();
  
  // attempt to solve the planning problem within one second of
  // planning time
  ob::PlannerStatus solved = optimizingPlanner->solve(10.0);

  if(solved) {
    ROS_INFO_STREAM("Solved"); 
    std::vector<og::PathGeometric> path(1, og::PathGeometric(si));
    path[0]=dynamic_cast<const og::PathGeometric& >( *pdef->getSolutionPath());
    path[0].interpolate(1000);
    pub_path_marker.publish(ompl_rviz::GetMarker(path[0], 0.3, 1, 0, 0, 1));

    ob::PlannerData data(si);
    optimizingPlanner->getPlannerData(data);
    pub_graph_marker.publish(ompl_rviz::GetGraph(data, 100, 0.05, 0, 0, 1, 0.3));
  }
  else  {
    ROS_INFO_STREAM("No Solution Found");
    ob::PlannerData data(si);
    optimizingPlanner->getPlannerData(data);
    pub_graph_marker.publish(ompl_rviz::GetGraph(data, 100, 0.05, 0, 0, 1, 0.3));
  }
  ros::Duration(1.0).sleep();


}

