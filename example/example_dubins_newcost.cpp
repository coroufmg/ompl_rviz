#include "ompl/base/spaces/DubinsStateSpace.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "ompl/base/ScopedState.h"
#include "ompl/base/objectives/PathLengthOptimizationObjective.h"
#include <ompl/base/objectives/VFUpstreamCriterionOptimizationObjective.h>
#include "ompl/geometric/PathSimplifier.h"
#include "ompl/base/DiscreteMotionValidator.h"
#include "ompl/geometric/planners/rrt/RRTstar.h"
#include "ompl/geometric/planners/rrt/RRTConnect.h"
#include "ompl/geometric/planners/rrt/pRRT.h"
#include "ompl/geometric/planners/rrt/VFRRT.h"
#include "ompl_rviz/ompl_rviz.h"
#include "ompl_rviz/rrtstar_field.h"

namespace ob = ompl::base;
namespace og = ompl::geometric;

class vec
	{
	  public:
  		double x, y; // Components of a vector
  		
		vec(double a, double b) : x(a), y(b)
			{}
  		
		void normalize(){
       			double module = sqrt(x*x+y*y);
       		   	x/=module;
       			y/=module;
  		}

		double innerProduct(vec a){
       			return x*a.x+y*a.y;
    			
  		}

		double module(){
       			return sqrt(x*x+y*y);
  		}

		vec sum(vec a){
       			vec b(x+a.x, y+a.y);
       			return b;

  		}

  		void multiply(double a){
       			  x*=a;
       			  y*=a;
  		}

	}; // class vec
	
class FollowVectorField : public ob::StateCostIntegralObjective 
	{
	  public:
		unsigned int dim; // dimension of the StateSpace
		
    		FollowVectorField(const ob::SpaceInformationPtr &si) : 
			ob::StateCostIntegralObjective(si, true) 
			{
			    dim = si_->getStateDimension();
			}
       
    		virtual ob::Cost motionCost(const ob::State *s1, const ob::State *s2) const; 
		
		og::PathGeometric integrateField(const ob::State *start, double delta, double radius_of_the_search_ball); 
    
    		virtual bool isSymmetric () const
		    	{
	   		   return false;
			}
    
    		virtual vec field(const ob::State *state) const
			{
			    vec v(1, 1);      // Simple 45 degrees, constant field
			    v.normalize();
  	   		    return v;
			}
		
	  }; // class FollowVectorField


ob::Cost FollowVectorField::motionCost(const ob::State *state1, const ob::State *state2) const
  {
    const ob::DubinsStateSpace::StateType *s1 = state1->as<ob::DubinsStateSpace::StateType>();
    const ob::DubinsStateSpace::StateType *s2 = state2->as<ob::DubinsStateSpace::StateType>();	
    double x=s2->getX()-s1->getX(); 
    double y=s2->getY()-s1->getY();	
    vec v(x, y);
    double module=v.module();
    v.normalize();
    vec u=field(s1); // Assuming it is already normalized
    ob::Cost c((1-u.innerProduct(v))*module);
    return c;
    
  } // motionCost

/** Gradient of the potential function 1 + sin(x[0]) * sin(x[1]). */

Eigen::VectorXd field(const ob::State *state)
{
   
  const ob::DubinsStateSpace::StateType &s = *state->as<ob::DubinsStateSpace::StateType>();
    Eigen::VectorXd v(2);
//     v[0] = std::cos(x.getX()) * std::sin(x.getY());
//     v[1] = std::sin(x.getX()) * std::cos(x.getY());
//     return -v; //example_field
    
     double x=s.getX();
     double y=s.getY();      
     double fi = pow(x,4) + pow(y,4) - 360000.0;
     Eigen::VectorXd gradfi(2);
     gradfi[0] = 10.0*(pow(x,3));
     gradfi[1] = 10.0*(pow(y,3));
     Eigen::VectorXd gradHfi(2);
     gradHfi[0] = -gradfi[1];
     gradHfi[1] = gradfi[0];
     double G = -0.636619772367581*atan(10*fi);
     double H = -sqrt(1.0-G*G);
     double h = 1.0/gradfi.norm();
 
     gradfi.normalize();  	
     gradfi = gradfi*(0.001*G);
     gradHfi = gradHfi*(H*h);
     
     Eigen::VectorXd v2=gradfi + gradHfi;
     v2.normalize();
     return v2;  //circular_field
    
// 	double d0=5;
// 	double a=0.2;
// 	double l=40;
// 	v[0]=1;
// 	v[1]=a*((d0-l/2)-s.getY());
// 	v.normalize();
// 	return v; //corridor_field
}
     

// Our collision checker. For this demo, our robot's state space
// lies in [0,1]x[0,1], with a circular obstacle of radius 0.25
// centered at (0.5,0.5). Any states lying in this circular region are
// considered "in collision".
class ValidityChecker : public ompl::base::StateValidityChecker
{
public:
    ValidityChecker(const ob::SpaceInformationPtr &si) :
        ompl::base::StateValidityChecker(si) {}
    // Returns whether the given state's position overlaps the
    // circular obstacle
    bool isValid(const ob::State* state) const
    {
        return this->clearance(state) > 0.0;
    }
    // Returns the distance from the given state's position to the
    // boundary of the circular obstacle.
    double clearance(const ob::State* state) const
    {
        // We know we're working with a DubinsStateSpace in this
        // example, so we downcast state into the specific type.
        const ob::DubinsStateSpace::StateType* state2D =
            state->as<ob::DubinsStateSpace::StateType>();
        // Extract the robot's (x,y) position from its state
        double x = state2D->getX();
        double y = state2D->getY();
	double z = state2D->getYaw();
        // Distance formula between two points, offset by the circle's
        // radius
        return sqrt((x-2.5)*(x-2.5) + (y-3.5)*(y-3.5)) - 0.00;
    }
};


// Returns a structure representing the optimization objective to use
// for optimal motion planning. This method returns an objective which
// attempts to minimize the length in configuration space of computed
// paths.
ob::OptimizationObjectivePtr getPathLengthObjective(const ob::SpaceInformationPtr &si)
{
    return ob::OptimizationObjectivePtr(new ob::PathLengthOptimizationObjective(si));
}


int main(int argc, char **argv) {
  ros::init(argc, argv, "ompl_rviz");
  ros::NodeHandle n("~");

  ros::Publisher pub_path_marker = n.advertise<visualization_msgs::Marker>("path", 1);
  ros::Publisher pub_graph_marker = n.advertise<visualization_msgs::Marker>("graph", 1);
  ros::Duration(1.0).sleep();

  // Construct the robot state space in which we're planning. We're
  // planning in [0,1]x[0,1], a subset of R^2.
  ob::StateSpacePtr space(new ob::DubinsStateSpace(2,true));

  // Set the bounds of space   
  ob::RealVectorBounds bounds(2);
  bounds.setLow(0, -30);  // x
  bounds.setLow(1, -30); // y 
  bounds.setHigh(0, 30); // x
  bounds.setHigh(1, 30); // y
  space->as<ob::DubinsStateSpace>()->setBounds(bounds);

  // Construct a space information instance for this state space
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));

  // Set the object used to check which states in the space are valid
  si->setStateValidityChecker(ob::StateValidityCheckerPtr(new ValidityChecker(si)));
  //si->setStateValidityCheckingResolution(0.0001);
  si->setup();

  // Set our robot's starting state coordinates
  ob::ScopedState<> start(space);
  start->as<ob::DubinsStateSpace::StateType>()->setX(-30.0);
  start->as<ob::DubinsStateSpace::StateType>()->setY(30.0);
  start->as<ob::DubinsStateSpace::StateType>()->setYaw(1.67);

  // Set our robot's goal state coordinates
  ob::ScopedState<> goal(space);
  goal->as<ob::DubinsStateSpace::StateType>()->setX(-30.0);
  goal->as<ob::DubinsStateSpace::StateType>()->setY(-30.0);
  goal->as<ob::DubinsStateSpace::StateType>()->setYaw(1.67);
  
  // Create a problem instance
  ob::ProblemDefinitionPtr pdef(new ob::ProblemDefinition(si));
 
  // Set the start and goal states
  pdef->setStartAndGoalStates(start, goal, 0.1);

  // Set the objective function
  //pdef->setOptimizationObjective(getPathLengthObjective(si));
  pdef->setOptimizationObjective(std::make_shared<ob::VFUpstreamCriterionOptimizationObjective>(si, field));
  
  // Construct our optimizing planner using the VFRRT algorithm.
 // double explorationSetting = 0.7;
 // double lambda = 1;
 // unsigned int update_freq = 100;
 // ob::PlannerPtr optimizingPlanner(new og::VFRRT(si, field, explorationSetting, lambda, update_freq));

 // Construct our optimizing planner using the RRTstar algorithm.
  ob::PlannerPtr optimizingPlanner(new og::RRTstar(si));
 
  // Set the problem instance for our planner to solve
  optimizingPlanner->setProblemDefinition(pdef);
  optimizingPlanner->setup();
  
  // attempt to solve the planning problem within one second of
  // planning time
  ob::PlannerStatus solved = optimizingPlanner->solve(30.0);

  if(solved) {
    ROS_INFO_STREAM("Solved"); 
    std::vector<og::PathGeometric> path(1, og::PathGeometric(si));
    path[0]=dynamic_cast<const og::PathGeometric& >( *pdef->getSolutionPath());
    path[0].interpolate(1000);
    pub_path_marker.publish(ompl_rviz::GetMarkerDubins(path[0], 0.5, 1, 0, 0, 1));

    ob::PlannerData data(si);
    optimizingPlanner->getPlannerData(data);
    pub_graph_marker.publish(ompl_rviz::GetGraphDubins(data, 100, 0.05, 0, 0, 1, 0.25));
  }
  else  {
    ROS_INFO_STREAM("No Solution Found");
    ob::PlannerData data(si);
    optimizingPlanner->getPlannerData(data);
    pub_graph_marker.publish(ompl_rviz::GetGraphDubins(data, 100, 0.05, 0, 0, 1, 0.3));
  }
  ros::Duration(1.0).sleep();


}
