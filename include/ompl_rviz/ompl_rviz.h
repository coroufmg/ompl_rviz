#include <ros/ros.h>
#include <visualization_msgs/MarkerArray.h>
#include "ompl/base/SpaceInformation.h"
#include "ompl/geometric/PathGeometric.h"
#include "ompl/base/PlannerData.h"
#include "ompl/base/PlannerDataGraph.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "ompl/base/spaces/DubinsStateSpace.h"
#include "ompl_rviz/rrtstar_field.h"

//path
#include <nav_msgs/Path.h>
#include <geometry_msgs/Pose2D.h>


#ifndef CORO__UFMG_OMPL_RVIZ_
#define CORO__UFMG_OMPL_RVIZ_

namespace ompl_rviz {

//publish a path using the mensage nav_msgs/Path
void publishPath(ros::Publisher& pub_path, ompl::geometric::PathGeometric& path, const std::string& frame_id = "world");

visualization_msgs::Marker GetMarker(boost::function< const ompl::base::State* (unsigned int)> GetStateFn,
                                     boost::function< std::size_t (void)> GetNumState,
                                     const ompl::base::SpaceInformationPtr& si,
                                     double scale, double r, double g, double b, double a, int dim = 2);

visualization_msgs::Marker GetMarker(const ompl::geometric::PathGeometric& path, double scale, double r, double g, double b, double a, int dim = 2);

visualization_msgs::Marker GetGraph(const ompl::base::PlannerData &data, int dim, double scale, double r, double g,  double b, double a);

void publishPathDubins(ros::Publisher& pub_path, ompl::geometric::PathGeometric& path, const std::string& frame_id = "world");

visualization_msgs::Marker GetMarkerDubins(const ompl::geometric::PathGeometric& path, double scale, double r, double g, double b, double a);

visualization_msgs::Marker GetMarkerDubins(boost::function< const ompl::base::State* (unsigned int)> GetStateFn,
                                     boost::function< std::size_t (void)> GetNumState,
                                     const ompl::base::SpaceInformationPtr& si,
                                     double scale, double r, double g, double b, double a);
  
visualization_msgs::Marker GetGraphDubins(const ompl::base::PlannerData &data, unsigned int resolution, double scale, double r, double g,  double b, double a);
  
}

#endif  // CORO__UFMG_OMPL_RVIZ_