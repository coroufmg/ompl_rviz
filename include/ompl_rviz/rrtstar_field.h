/* Copyright 2014 Guilherme Pereira
 * rrtstar_field.cpp
 *
 *  Created on: Dec 09, 2015
 *      Author: Guilherme Pereira
 */

#ifndef SAMPLING_PLANNERS_INCLUDE_SAMPLING_PLANNERS_RRTSTAR_FIELD_H_
#define SAMPLING_PLANNERS_INCLUDE_SAMPLING_PLANNERS_RRTSTAR_FIELD_H_

#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "ompl/base/ScopedState.h"
#include "ompl/base/objectives/StateCostIntegralObjective.h"
#include "ompl/geometric/planners/rrt/RRTstar.h"
#include <Eigen/Core>

namespace ob = ompl::base;
namespace og = ompl::geometric;

namespace ca {
   namespace rrtstar_field {


	// Class to encapsulate a vector in 2D
	class vec
	{
	  public:
  		double x, y; // Components of a vector
  		
		vec(double a, double b) : x(a), y(b)
			{}
  		
		void normalize(){
       			double module = sqrt(x*x+y*y);
       		   	x/=module;
       			y/=module;
  		}

		double innerProduct(vec a){
       			return x*a.x+y*a.y;
    			
  		}

		double module(){
       			return sqrt(x*x+y*y);
  		}

		vec sum(vec a){
       			vec b(x+a.x, y+a.y);
       			return b;

  		}

  		void multiply(double a){
       			  x*=a;
       			  y*=a;
  		}

	}; // class vec

	// Class to encapsulate a vector in 3D
	class vec3
	{
	  public:
  		double x, y, z; // Components of a vector
  		  		
		vec3(double a, double b, double c) : x(a), y(b), z(c)
			{}
  		
		void normalize(){
    			double module = sqrt(x*x+y*y+z*z);
       			x/=module;
                        y/=module;
                        z/=module;
  		}

		double innerProduct(vec3 a){
    			return x*a.x+y*a.y+z*a.z;	
  		}

		double module(){
 			return sqrt(x*x+y*y+z*z);
  		}

		vec3 sum(vec3 a){
       			vec3 b(x+a.x, y+a.y, z+a.z);  
       			return b;
  		}

		vec3 cross(vec3 a){
			vec3 b(y * a.z - z * a.y, z * a.x - x * a.z, x * a.y - y * a.x);  
       			return b;
  		}

  		void multiply(double a){
  			  x*=a;
       			  y*=a;
       			  z*=a;
  		}

	}; // class vec3

	// Class to define the new cost function
	class FollowVectorField : public ob::StateCostIntegralObjective 
	{
	  public:
		unsigned int dim; // dimension of the StateSpace
		
    		FollowVectorField(const ob::SpaceInformationPtr &si) : 
			ob::StateCostIntegralObjective(si, true) 
			{
			    dim = si_->getStateDimension();
			}
       
    		virtual ob::Cost motionCost(const ob::State *s1, const ob::State *s2) const; 
		
		og::PathGeometric integrateField(const ob::State *start, double delta, double radius_of_the_search_ball); 
    
    		virtual bool isSymmetric () const
		    	{
	   		   return false;
			}
    
    		virtual vec field(const ob::State *state) const
			{
			    vec v(1, 1);      // Simple 45 degrees, constant field
			    v.normalize();
  	   		    return v;
			}
	
		virtual vec3 field3(const ob::State *state) const
			{
	   		    vec3 v(1, 1, 1);
			    v.normalize();
  	   		    return v;	  
			}
			
		
	  }; // class FollowVectorField

	// Class to define the new cost function in 3D
	class FollowVectorField3 : public ob::StateCostIntegralObjective 
	{
	  public:
		unsigned int dim; // dimension of the StateSpace
		
    		FollowVectorField3(const ob::SpaceInformationPtr &si) : 
			ob::StateCostIntegralObjective(si, true) 
			{
			    dim = si_->getStateDimension();
			}
       
    		virtual ob::Cost motionCost(const ob::State *s1, const ob::State *s2) const; 
    
    		virtual bool isSymmetric () const
		    	{
	   		   return false;
			}
    
		virtual vec3 field(const ob::State *state) const
			{
	   		    vec3 v(1, 1, 1);
			    v.normalize();
  	   		    return v;	  
			}	
	  }; // class FollowVectorField3


	// Class RRTstarField created to allow the redefinition of getPlannerData 
	// The new getPlannerData allows solutions without a pre-specified goal.
	// We recompute the path inside getPlannerData. The original path is still computed 
	// inside the solve() method. Therefore, getPlannerData must run before getting the 
	// path, otherwise we get the old path, which considers the goal.
	class RRTstarField : public og::RRTstar
	{

  	  double ball_radius;
	  double delta_radius;
	  double closeness_ratio;
	  mutable bool foundPath_;

	  public:
	
    		RRTstarField(const ob::SpaceInformationPtr &si) : 
		          og::RRTstar(si) {foundPath_=false;}    
 
		bool computeFinalPath() const;
		
		bool foundPath() const
			{
			  return foundPath_;
			}
		
		void setBallRadius(double radius, double delta, double acceptable_closeness_to_the_border = 0.5) 
			{ 
		  	  ball_radius=radius;
   		  	  delta_radius=delta;
			  closeness_ratio=acceptable_closeness_to_the_border;
			}
				
	  protected:
	    
	      void setFoundPath() const
		      {
			foundPath_=true;
		      }
	      void resetFoundPath() const
		      {
			foundPath_=false;
		      }
	      
	};  //class RRTstarField


	// This class was created to allow the redefinition of sampleUniform.
	// The new sampleUniform samples inside a ball.
	class RealVectorStateSamplerCircular : public ob::RealVectorStateSampler
   	{
      	   public:
	
           	RealVectorStateSamplerCircular(const ob::StateSpace *space) : 
		         ob::RealVectorStateSampler(space) {}
    
            	virtual void sampleUniform(ob::State *state);     
   	}; // class RealVectorStateSamplerCircular


	// This class was created to allow the redefinition of sampleUniform.
	// The new sampleUniform samples inside a ball.
	class RealVectorStateSamplerCircular3 : public ob::RealVectorStateSampler
   	{
      	   public:
	
           	RealVectorStateSamplerCircular3(const ob::StateSpace *space) : 
		         ob::RealVectorStateSampler(space) {}
    
            	virtual void sampleUniform(ob::State *state);     
   	}; // class RealVectorStateSamplerCircular


	// This class was created to allow the redefinition of allocDefaultStateSampler.
	// The new allocDefaultStateSamples will be calling the new sampling function that samples inside a ball.
	class RealVectorStateSpaceField : public ob::RealVectorStateSpace
    	{
           double ball_radius;
           ob::RealVectorStateSpace::StateType *startState;

           public:
		            	
		RealVectorStateSpaceField(unsigned int dim = 0, double radius=10.0) : ob::RealVectorStateSpace(dim), ball_radius(radius)
			{startState=NULL;}
		
		virtual ob::StateSamplerPtr allocDefaultStateSampler() const;
		
		ob::RealVectorStateSpace::StateType* getStart() const
			{	
                           if (startState == NULL)
				OMPL_INFORM("startState in RealVectorStateSpaceField was not defined!"); 
			   return startState;
			}

		double getBallRadius() const
			{
			   return ball_radius;
			}

		void setStart(const ob::ScopedState<> &start)
			{	
			   OMPL_INFORM("Defining Start!"); 	
			   startState = (ob::RealVectorStateSpace::StateType*)start.get();
			}

		void setBallRadius(double radius) 
	      		{ 
	     	  	   ball_radius=radius;
	     		}	
					
   	}; // class RealVectorStateSpaceField



   } // namespace rrtstar_field
} // namespace ca



#endif  // SAMPLING_PLANNERS_INCLUDE_SAMPLING_PLANNERS_RRTSTAR_FIELD_H_ 
