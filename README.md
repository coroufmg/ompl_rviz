# ompl_rviz #

This repository provides functions that allow the visualization of OMPL paths and graphs in RVIZ. It also provides a stand alone example of OMPL.


### How do I get set up? ###


Create a workspace: 

```
#!bash

source /opt/ros/indigo/setup.bash # this is usually not necessary since it is done inside .bashrc 
mkdir -p ~/ompl_rviz_ws/src
cd ompl_rviz_ws/src
catkin_init_workspace

```

Download the package:



```
#!bash
git clone git@bitbucket.org:coroufmg/ompl_rviz.git

```

Install OMPL (Open Motion Planning Library) using the instructions on the [official home page](http://ompl.kavrakilab.org/download.html) 


Compile:


```
#!bash

cd ..
catkin_make
```

Run an example:

```
#!bash

source devel/setup.bash
roslaunch ompl_rviz example.launch 

```


### Who do I talk to? ###

* Guilherme Pereira (gpereira@ufmg.br)


### How to install OMPL on Odroid XU4? ###

Download the OMPL installation script. First, make the script executable: 

```
#!bash
chmod u+x install-ompl-ubuntu.sh
./install-ompl-ubuntu.sh
```
*Ref.: http://ompl.kavrakilab.org/installation.html*

In your OMPL build directory: 
```
#!bash
cmake -DCMAKE_INSTALL_PREFIX=/usr
sudo make install
```
*Ref.: http://ompl.kavrakilab.org/buildSystem.html*

Reboot the Odroid.

### Troubleshooting

- If you have a issue to find Eigen library with this error message: "Eigen/Core: No such file or directory compilation terminated." 
You can execute this command to solve the problem:
```
#!bash
sudo ln -s /usr/include/eigen3/Eigen /usr/local/include/Eigen
```
