#include "ompl_rviz/ompl_rviz.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"

namespace ob = ompl::base;

namespace ompl_rviz {
  
//publish a path using the mensage nav_msgs/Path
void publishPath(ros::Publisher& pub_path, ompl::geometric::PathGeometric& path, const std::string& frame_id){
    nav_msgs::Path path_to_publish;
    path_to_publish.poses.resize(path.getStateCount());
    geometry_msgs::PoseStamped pose;
    std::vector<geometry_msgs::PoseStamped> plan;
    path_to_publish.header.frame_id = frame_id;
    path_to_publish.header.stamp = ros::Time(0);
    for(int ii=0; ii < path.getStateCount(); ii++){		    
      //get the state i
      ob::State* state = path.getState(ii);    		    
      
      pose.pose.position.x = state->as<ob::RealVectorStateSpace::StateType>()->values[0];
      pose.pose.position.y = state->as<ob::RealVectorStateSpace::StateType>()->values[1];
      pose.pose.position.z = 0.0;
      pose.pose.orientation.x = 0.0;
      pose.pose.orientation.y = 0.0;
      pose.pose.orientation.z = 0.0;
      pose.pose.orientation.w = 1.0;
      plan.push_back(pose);
      path_to_publish.poses[ii] = plan[ii];		    
      
    }
    pub_path.publish(path_to_publish);

}

visualization_msgs::Marker GetMarker(boost::function< const ompl::base::State* (unsigned int)> GetStateFn,
                                     boost::function< std::size_t (void)> GetNumState,
                                     const ompl::base::SpaceInformationPtr& si,
                                     double scale, double r, double g, double b, double a, int dim) {

  visualization_msgs::Marker m;
  m.header.frame_id = "world";
  m.header.stamp = ros::Time::now();
  m.ns = "path";
  m.id = 0;
  m.action = visualization_msgs::Marker::ADD;
  m.scale.x = scale;
  m.type = visualization_msgs::Marker::LINE_STRIP; //correct marker type
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;

  // Fill up msg and return it
  geometry_msgs::Point ps;
  ps.z = 0.0;
  for (std::size_t i = 0 ; i < GetNumState(); i++) {
    ps.x = GetStateFn(i)->as<ob::RealVectorStateSpace::StateType>()->values[0];
    ps.y = GetStateFn(i)->as<ob::RealVectorStateSpace::StateType>()->values[1];
    if(dim==3)
      ps.z = GetStateFn(i)->as<ob::RealVectorStateSpace::StateType>()->values[2]; // we need to change something for 3D
    m.points.push_back(ps);
  }
  return m;

}

visualization_msgs::Marker GetMarker(const ompl::geometric::PathGeometric& path, double scale, double r, double g, double b, double a, int dim) {
  return GetMarker( boost::bind( static_cast< const ompl::base::State* (ompl::geometric::PathGeometric::*)( unsigned int ) const > (&ompl::geometric::PathGeometric::getState), &path, _1),
                    boost::bind(&ompl::geometric::PathGeometric::getStateCount, &path),
                    path.getSpaceInformation(),
                    scale, r, g, b, a, dim);

}

visualization_msgs::Marker GetGraph(const ompl::base::PlannerData &data, int dim, double scale, double r, double g,  double b, double a) {
  namespace ob = ompl::base;
  visualization_msgs::Marker m;
  m.action = visualization_msgs::Marker::ADD;
  m.scale.x = scale;
  m.type = visualization_msgs::Marker::LINE_LIST; //correct marker type
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;

  m.header.frame_id = "world";
  m.ns = "graph";
  m.id = 0;
  m.header.stamp = ros::Time::now();
  m.scale.x = scale;
  //m.scale.y = 1.5*scale;
  m.scale.y = scale;
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;

  // Get graph
  ob::PlannerData::Graph::Type graph = data.toBoostGraph();
  ob::PlannerData::Graph::EIterator ei, ei_end;
  boost::property_map<ob::PlannerData::Graph::Type, vertex_type_t>::type vertices = get(vertex_type_t(), graph);
  // Fill up msg and return
  geometry_msgs::Point ps;
  ps.z = 0.0;
  for (boost::tie(ei, ei_end) = boost::edges(graph); ei != ei_end; ++ei) {
    // Get path corresponding to edge
    std::vector<ob::State*> block;
    unsigned int ans = data.getSpaceInformation()->getMotionStates(vertices[boost::source(*ei, graph)]->getState(), vertices[boost::target(*ei, graph)]->getState(), block, 0, true, true);
    BOOST_ASSERT_MSG((int)ans == ans && block.size() == ans, "Internal error in path interpolation. Incorrect number of intermediate states. Please contact the developers.");
    // Enter path in marker
    for (std::vector<ob::State*>::iterator it = block.begin() ; it != block.end(); ++it) {
      
      ps.x = (*it)->as<ob::RealVectorStateSpace::StateType>()->values[0];
      ps.y = (*it)->as<ob::RealVectorStateSpace::StateType>()->values[1];
      
      if(dim==3)
	ps.z = (*it)->as<ob::RealVectorStateSpace::StateType>()->values[2]; // we need to change something for 3D
	
      m.points.push_back(ps);
      if (it != block.begin() && boost::next(it) != block.end())
        m.points.push_back(ps);
    }
    
    // prevents memory overflow
    data.getSpaceInformation()->freeStates(block);
    
  }
  graph.clear();
  return m;
}

//DubinsStateSpace
void publishPathDubins(ros::Publisher& pub_path, ompl::geometric::PathGeometric& path, const std::string& frame_id){
    nav_msgs::Path path_to_publish;
    path_to_publish.poses.resize(path.getStateCount());
    geometry_msgs::PoseStamped pose;
    std::vector<geometry_msgs::PoseStamped> plan;
    path_to_publish.header.frame_id = frame_id;
    path_to_publish.header.stamp = ros::Time(0);
    for(int ii=0; ii <= path.getStateCount(); ii++){		    
      //get the state i
      ob::State* state = path.getState(ii);    		    
      
      pose.pose.position.x = state->as<ompl::base::DubinsStateSpace::StateType>()->getX();
      pose.pose.position.y = state->as<ompl::base::DubinsStateSpace::StateType>()->getY();
      pose.pose.position.z = state->as<ompl::base::DubinsStateSpace::StateType>()->getYaw();
      pose.pose.orientation.x = 0.0;
      pose.pose.orientation.y = 0.0;
      pose.pose.orientation.z = 0.0;
      pose.pose.orientation.w = 1.0;
      plan.push_back(pose);
      path_to_publish.poses[ii] = plan[ii];		    
      
    }
    pub_path.publish(path_to_publish);

}
visualization_msgs::Marker GetMarkerDubins(const ompl::geometric::PathGeometric& path, double scale, double r, double g, double b, double a) {
  return GetMarkerDubins( boost::bind( static_cast< const ompl::base::State* (ompl::geometric::PathGeometric::*)( unsigned int ) const > (&ompl::geometric::PathGeometric::getState), &path, _1),
                    boost::bind(&ompl::geometric::PathGeometric::getStateCount, &path),
                    path.getSpaceInformation(),
                    scale, r, g, b, a);
}

visualization_msgs::Marker GetMarkerDubins(boost::function< const ompl::base::State* (unsigned int)> GetStateFn,
                                     boost::function< std::size_t (void)> GetNumState,
                                     const ompl::base::SpaceInformationPtr& si,
                                     double scale, double r, double g, double b, double a) {

  visualization_msgs::Marker m;
  m.header.frame_id = "world";
  m.header.stamp = ros::Time::now();
  m.ns = "path";
  m.id = 0;
  m.action = visualization_msgs::Marker::ADD;
  m.scale.x = scale;
  m.type = visualization_msgs::Marker::LINE_STRIP; //correct marker type
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;

  // Fill up msg and return it
  geometry_msgs::Point ps;
  for (std::size_t i = 0 ; i < GetNumState(); i++) {
    const ob::State *state =GetStateFn(i);
    const ob::DubinsStateSpace::StateType *s = state->as<ob::DubinsStateSpace::StateType>();
    ps.x = s->getX();
    ps.y = s->getY();
    ps.z = s->getYaw(); 
    m.points.push_back(ps);
  }
  return m;

}

visualization_msgs::Marker GetGraphDubins(const ompl::base::PlannerData &data, unsigned int resolution, double scale, double r, double g,  double b, double a) {
  namespace ob = ompl::base;
  visualization_msgs::Marker m;
  m.action = visualization_msgs::Marker::ADD;
  m.scale.x = scale;
  m.type = visualization_msgs::Marker::LINE_LIST; //correct marker type
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;

  m.header.frame_id = "world";
  m.ns = "graph";
  m.id = 0;
  m.header.stamp = ros::Time::now();
  m.scale.x = scale;
  m.scale.y = 1.5*scale;
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;

  // Get graph
  ob::PlannerData::Graph::Type graph = data.toBoostGraph();
  ob::PlannerData::Graph::EIterator ei, ei_end;
  boost::property_map<ob::PlannerData::Graph::Type, vertex_type_t>::type vertices = get(vertex_type_t(), graph);
  // Fill up msg and return
  geometry_msgs::Point ps;
  for (boost::tie(ei, ei_end) = boost::edges(graph); ei != ei_end; ++ei) {
    // Get path corresponding to edge
    std::vector<ob::State*> block;
    unsigned int ans = data.getSpaceInformation()->getMotionStates(vertices[boost::source(*ei, graph)]->getState(), vertices[boost::target(*ei, graph)]->getState(), block, resolution, true, true);
    BOOST_ASSERT_MSG((int)ans == ans && block.size() == ans, "Internal error in path interpolation. Incorrect number of intermediate states. Please contact the developers.");
    // Enter path in marker
    for (std::vector<ob::State*>::iterator it = block.begin() ; it != block.end(); ++it) {
      ob::State *state =*it;
      const ob::DubinsStateSpace::StateType *s = state->as<ob::DubinsStateSpace::StateType>();
      ps.x = s->getX();
      ps.y = s->getY();
      ps.z = s->getYaw(); 
      m.points.push_back(ps);
      if (it != block.begin() && boost::next(it) != block.end())
        m.points.push_back(ps);
    }
     data.getSpaceInformation()->freeStates(block);
  }
  graph.clear();
  return m;
}


} // namespace ompl_rviz 